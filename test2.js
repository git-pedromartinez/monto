/*

Input:   str = "a,b$c"

Output:  str = "c,b$a"

Note that $ and , are not moved anywhere. 

Only subsequence "abc" is reversed

Input:   str = "Ab,c,de!$"

Output:  str = "ed,c,bA!$"

*/

function pali(cadena) {
    if (typeof cadena === "string") {
        var cadena_final = cadena.split("").map(char => {
            if (esAlfabeto(char)) {
                return ''
            } else {
                return char
            }
        });
        var j = cadena.length - 1;
        while (j >= 0) {
            if (esAlfabeto(cadena[j])) {
                cadena_final[cadena_final.findIndex((char) => char === '')]=cadena[j]
            }
            j--;
        }
        return cadena_final.join().replace(/,,,/g,'¬').replace(/,/g,"").replace(/¬/g,",");
    }
    return null;
}

function esAlfabeto(caracter) {
    var alfabeto = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    alfabeto += alfabeto.toLocaleLowerCase();
    if (alfabeto.indexOf(caracter) !== -1) {
        return true;
    }
    return false;
}

//case 1
var input = "a,b$c";
console.log(input);
console.log(pali(input));

//case 2
// var input = "Ab,c,de!$";
// console.log(input);
// console.log(pali(input));
